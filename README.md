# Arduino Subsystem


## Tujuan dan Fungsi Subsistem (Arduino)
Tujuan dari subsistem arduino adalah untuk mendukung subsistem utama yaitu pada unity (platform game). Fungsi subsistem arduino yaitu memberikan data mengenai kecerahan cahaya disekitarnya dan menampilkan status level pengguna.


## Daftar Fitur
Adapun fitur-fitur yang tersedia dalam arduino sangat minimalis, yaitu sebagai berikut:


1) Memiliki sensor cahaya untuk mendeteksi intensitas cahaya disekitarnya yang akan digunakan unity mengatur tingkat kecerahan dalam permainan.


2) Menampilkan status level pengguna yang akan ditampilkan pada 7 segment.


3) LCD menampilkan nama user yang sedang memainkan game..


4) Tombol Switch dapat menghidupkan dan mematikan LCD.


5) LED dapat mengedipkan setiap detik ketika pemain telah naik level.


## Daftar Komponen
Komponen yang digunakan sebagai berikut,


1) 1 buah LCD tipe 1602


2) 3 buah breadboard


3) Arduino UNO v3.0


4) 2 buah seven-segment


5) 2 buah Resistor 330 Ω


6) Kabel male-female secukupnya


7) 3 buah Integrated Circuit (IC) ( 2 IC binary convert to decimal, 1 IC save register IDSN741S164N )


8) 1 buah LED Kuning


9) 1 buah push button


10) 1 buah IC PCP8574T untuk LCD

## Mekanisme Komunikasi dengan Platform Lain
Arduino akan berkomunikasi melalui Kabel Serial untuk memberikan data intensitas cahaya kepada game di unity.Komunikasi ini dilakukan melalui serial port COM5. Arduino akan mendapatkan data status level pemain dan beberapa nama layer yang sedang memainkan game untuk ditampilkan di 7-Segmen dan LCD.

## Board Diagram
Pada Gambar dibawah akan diperlihatkan susunan Board yang akan dibuat pada tugas ini. Jika menginginkan sumber gambar atau memodifikasi gambar, dapat dilihat di http://fritzing.org/projects/pbd-tubes-milestone-3.


![board](screenshot/board.png)

Gambar Board Diagram
## Foto Sistem
Dalam Gambar-gambar dibawah dilihat perbedaan state pada LCD. Perbedaan state ini akan terjadi saat tombol ditekan. Dalam layar akan ditampilkan nama pemain.


![arduino1](screenshot/arduino1.jpg)

Gambar Sistem Saat LCD Dimatikan

![arduino2](screenshot/arduino2.jpg)

Gambar Sistem Saat LCD Dinyalakan

# About

1. Bervianto Leo P - 13514047
2. Ade Surya Ramadhani - 13514049
3. M. Az-zahid Adhitya Silparensi - 13514095
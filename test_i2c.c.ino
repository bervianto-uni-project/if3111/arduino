#include <LiquidCrystal_I2C.h>
#define I2C_ADDR 0x3F
#define pinSensor 0
#define pinLED 5
#define pin7segmentClock 13
#define pin7segmentData 12
#define pinClock 13
#define pinData 12

int pwmVal;
int incrementor;
LiquidCrystal_I2C lcd(I2C_ADDR, 16, 2);
 
void setup()
{
  
  lcd.begin();
  lcd.backlight();
  lcd.print("Hello, world!");
   // put your setup code here, to run once:
  pinMode(pinClock, OUTPUT);
  pinMode(pinData, OUTPUT);
  digitalWrite(pinClock, 1);
  digitalWrite(pinData, 0);
  pwmVal = 11;
  incrementor = 20;
  Serial.begin(9600);
}

int a = 0;
int data[8] = {0,0,0,1,1,0,0,1};

void loop()
{
  int senseVal; 
  String stringWorld = String("Hello Sense ");
  senseVal = analogRead(pinSensor);
  stringWorld = String(stringWorld + String(senseVal));
  lcd.clear();
  lcd.print(stringWorld);

  //PWM
  if(pwmVal <= 20 || pwmVal > 235){
    incrementor = -1*incrementor;
  }

  pwmVal += incrementor;

  analogWrite(pinLED, pwmVal);

   // put your main code here, to run repeatedly:
  
  for (int i=0; i<8; i++) {
    digitalWrite(pinClock, 0);
    digitalWrite(pinData, data[i]);
    digitalWrite(pinClock, 1);
  }
  

  delay(1000);
}
